const GenderPredictionModel = require("../model/GenderPredictionModel")

const methods = {
    predictGender: function (req, res) {
        const model = new GenderPredictionModel();
        return model.executeModel();
    }
}

module.exports = methods;
