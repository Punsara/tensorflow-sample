const express = require('express');
const routes = require("./routes/index");

const app = express();
app.use(routes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`));
