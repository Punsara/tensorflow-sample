const tf = require("@tensorflow/tfjs");

class GenderPredictionModel {
    compileModel() {

        const model = tf.sequential();

        // Input Layer
        model.add(tf.layers.dense({
            units: 2,
            inputShape: [2]
        }));

        // Dense Layer 1
        model.add(tf.layers.dense({
            units: 16,
            activation: 'relu'
        }));

        // Dropout Layer 1
        model.add(tf.layers.dropout({
            args: 0.2
        }));

        // Dense Layer 2
        model.add(tf.layers.dense({
            units: 8,
            activation: 'relu'
        }));

        // Dropout Layer 2
        model.add(tf.layers.dropout({
            args: 0.2
        }));

        // output Layer
        model.add(tf.layers.dense({
            units: 1,
            activation: 'sigmoid'
        }));

        model.compile({
            loss: 'meanSquaredError',
            optimizer: 'sgd',
            metrics: ['accuracy']
        });

        return model;
    }

    executeModel() {
        const model = this.compileModel();
        const inputXLayerData = [
            // Male
            [73.84701702,	241.8935632],
            [68.78190405,	162.3104725],
            [74.11010539,	212.7408556],
            [71.7309784,	220.0424703],
            [69.88179586,	206.3498006],
            [67.25301569,	152.2121558],
            [68.78508125,	183.9278886],
            [68.34851551,	167.9711105],
            [67.01894966,	175.9294404],
            [63.45649398,	156.3996764],
            [71.19538228,	186.6049256],
            [71.64080512,	213.7411695],
            [64.76632913,	167.1274611],
            [69.2830701,	189.4461814],
            [69.24373223,	186.434168],
            [67.6456197,	172.1869301],
            [72.41831663,	196.0285063],
            [63.97432572,	172.8834702],
            [69.6400599,	185.9839576],
            [67.93600485,	182.426648],

            // Female
            [58.91073204,	102.0883264],
            [65.23001251,	141.3058226],
            [63.36900376,	131.0414027],
            [64.47999743,	128.1715112],
            [61.79309615,	129.781407],
            [65.96801895,	156.8020826],
            [62.85037864,	114.9690383],
            [65.65215644,	165.0830012],
            [61.89023374,	111.6761992],
            [63.67786815,	104.1515596],
            [68.10117224,	166.5756608],
            [61.79887853,	106.233687],
            [63.37145896,	128.1181691],
            [58.89588635,	101.6826134],
            [58.4382491,	98.19262093],
            [60.80979868,	126.9154633],
            [70.12865283,	151.2542704],
            [62.25742965,	115.7973934],
            [61.73509022,	107.8668724],
            [63.05955669,	145.5899291]
        ];

        // Male as 0 and Female as 1
        const inputYLayerData = [
            [0],
            [0],
            [0],
            [0],
            [0],
            [0], [0], [0], [0], [0],
            [0], [0], [0], [0], [0],
            [0], [0], [0], [0], [0],

            [1], [1], [1], [1], [1],
            [1], [1], [1], [1], [1],
            [1], [1], [1], [1], [1],
            [1], [1], [1], [1], [1],
        ];

        // xs - input layer
        const xs = tf.tensor2d(inputXLayerData);

        // ys - input layer
        const ys = tf.tensor(inputYLayerData);

        model.fit(xs, ys, {epochs: 1000}).then(r => {
            const data = tf.tensor2d([[58.89588635,	101.6826134]]);

            let prediction = model.predict(data);
            const result = prediction.print();
            console.log(result);
            // console.log(result > 0.5 ? 'Female' : 'Male');
        });
    }
}

module.exports = GenderPredictionModel;
