const express = require('express');

const services = require("../services/index");
const router = express.Router();

router.get("/api/predictGender", services.predictGender);

module.exports = router;
